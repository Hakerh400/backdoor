import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Prelude hiding (log)
import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import qualified Data.ByteString as BS
import Data.Functor.Identity
import Data.Time.Clock
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except
import System.Directory
import System.Environment as Env

import Util
import Bit

main :: IO ()
main = do
  nop