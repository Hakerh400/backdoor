@echo off
cls

cd src
ghc -outputdir "../build" -o "../main.exe" Main.hs
cd ..